<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik',16);
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->enum('jk', ['L', 'P']);
            $table->enum('gol_darah',['A','B','AB','O']);
            $table->string('alamat',100);
            $table->string('rt',3);
            $table->string('rw',3);
            $table->string('kelurahan',100);
            $table->string('kecamatan',100);
            $table->enum('agama',['islam','buddha','kristen','katholik','hindu','konghucu']);
            $table->enum('status',['bk','k','cm','ch']);
            $table->string('pekerjaan',50);
            $table->enum('kwn',['wna','wni']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
