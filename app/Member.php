<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'nik', 
        'nama', 
        'tempat_lahir', 
        'tgl_lahir', 
        'jk', 
        'gol_darah', 
        'alamat', 
        'rt', 
        'rw', 
        'kelurahan', 
        'kecamatan', 
        'agama',
        'status', 
        'pekerjaan', 
        'kwn',
    ];


}
