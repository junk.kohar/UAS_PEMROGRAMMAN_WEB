@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Data KTP Member</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('members.create') }}"> Tambah Data Baru</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>NIK</th>
            <th>Nama</th>
            <th>Tempat , Tanggal Lahir</th>
            <th width="280px">Pilihan</th>
        </tr>
    @foreach ($members as $member)
    <tr>
        <td>{{ $member->nik }}</td>
        <td>{{ $member->nama}}</td>
        <td>{{ $member->tempat_lahir.', '.$member->tgl_lahir }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('members.show',$member->id) }}">Lihat</a>
            <a class="btn btn-primary" href="{{ route('members.edit',$member->id) }}">Ubah</a>
            {!! Form::open(['method' => 'DELETE','route' => ['members.destroy', $member->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Hapus', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $members->render() !!}
@endsection