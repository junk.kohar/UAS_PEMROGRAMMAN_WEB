@extends('layouts.default')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Lihat Member</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('members.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NIK:</strong>
                {!! Form::text('nik', $member->nik , array('placeholder' => 'NIK', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                {!! Form::text('nama', $member->nama, array('placeholder' => 'Nama', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tempat Lahir:</strong>
                {!! Form::text('tempat_lahir', $member->tempat_lahir, array('placeholder' => 'Tempat Lahir', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tanggal Lahir:</strong>
                {!! Form::date('tgl_lahir', $member->tgl_lahir, array('placeholder' => 'Tanggal Lahir', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jenis Kelamin:</strong>
                {!! Form::select('jk', trans('option.jk'), $member->jk,  array( 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Golongan Darah:</strong>
                {!! Form::select('gol_darah', trans('option.gol_darah'), $member->gol_darah,  array( 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                {!! Form::textarea('alamat', $member->alamat, array('placeholder' => 'Alamat', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
    
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Rt:</strong>
                {!! Form::number('rt', $member->rt, array('placeholder' => 'RT', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Rw:</strong>
                {!! Form::number('rw', $member->rw, array('placeholder' => 'RW', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kelurahan:</strong>
                {!! Form::text('kelurahan', $member->kelurahan, array('placeholder' => 'Kelurahan', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kecamatan:</strong>
                {!! Form::text('kecamatan', $member->kecamatan, array('placeholder' => 'Kecamatan', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
    
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Agama:</strong>
                {!! Form::select('agama', trans('option.agama'), $member->agama,  array( 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                {!! Form::select('status', trans('option.status'), $member->status,  array( 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Pekerjaan:</strong>
                {!! Form::text('pekerjaan', $member->pekerjaan, array('placeholder' => 'Pekerjaan', 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kewarganegaraan:</strong>
                {!! Form::select('kwn', trans('option.kwn'), $member->kwn,  array( 'disabled' => true, 'class' => 'form-control')) !!}
            </div>
        </div>
        
    </div>
@endsection