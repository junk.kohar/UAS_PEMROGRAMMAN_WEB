<?php

return [
    'jk' => [
        'L' => 'Laki-Laki',
        'P' => 'Perempuan',
    ],
    'gol_darah' => [
        'A' => 'A',
        'B' => 'B',
        'AB'    => 'AB',
        'O' => 'O',
    ],
    'agama' => [
        'islam' => 'Islam',
        'buddha' => 'Buddha',
        'kristen' => 'Kristen',
        'katholik' => 'Khatolik',
        'hindu' => 'Hindu',
        'konghucu' => 'Konghucu',
    ],
    'status' => [
        'bk' => 'Belum Kawin',
        'k' => 'Kawin',
        'cm' => 'Cerai Mati',
        'ch' => 'Cerai Hidup',
    ],
    'kwn'   => [
        'wni'   => 'Warga Negawa Indonesia',
        'wna'   => 'Warga Negara Asing'
    ],
];
           
            $table->enum('kwn',['wna','wni']);